const electron = require("electron")
const path = require("path")
const BrowserWindow = electron.remote.BrowserWindow

// For debugging
// console.log("r/place") // eslint-disable-line no-console

const canvas = document.querySelector("canvas")
canvas.width = window.innerWidth
canvas.height = window.innerHeight

var c = canvas.getContext("2d")

c.fillRect(100, 100, 250, 100)
c.beginPath()
c.moveTo(50, 300)
c.lineTo(300, 100)
c.stroke();
